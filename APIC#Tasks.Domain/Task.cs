﻿namespace APIC_Tasks.Domain
{
    public class Task
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Title is missing")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Creation date is missing")]
        public DateTime CreationDate { get; set; }
        [Required(ErrorMessage = "Finish date is missing")]
        public DateTime DueDate { get; set; }
        public bool IsCompleted { get; set; }
    }
}
