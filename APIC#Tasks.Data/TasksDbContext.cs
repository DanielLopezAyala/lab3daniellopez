﻿namespace APIC_Tasks.Data;

public class TasksDbContext : DbContext
    {
        public DbSet<Task> Tasks { get; set; }

        public TasksDbContext(DbContextOptions<TasksDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
