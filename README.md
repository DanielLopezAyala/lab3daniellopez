## ¡Gestiona tus tareas con facilidad con la API de Tareas!

**¿Necesitas una forma simple y eficaz de organizar tus tareas?** ¡No busques más! La API de Tareas te ofrece todo lo que necesitas para gestionar tus tareas desde cualquier lugar.

**¿Qué puedes hacer con la API de Tareas?**

- **Crear:** Agrega nuevas tareas a tu lista con solo unos pocos clics.
- **Leer:** Consulta todas tus tareas, pendientes y completadas, en un solo lugar.
- **Actualizar:** Modifica los detalles de las tareas existentes, como la fecha límite o la prioridad.
- **Eliminar:** Elimina las tareas que ya no necesitas.

**¿Qué necesitas para empezar?**

- Tener instalado .NET 8.0 o superior.
- Contar con una base de datos MySQL (o compatible).
- Un sistema operativo Linux (o compatible con .NET).

**¿Cómo empezar a usarla?**

1. Clona el repositorio del proyecto en tu equipo.
2. Configura tu base de datos MySQL y actualiza la información de conexión en el archivo `appsettings.json`.
3. Ejecuta el comando `dotnet ef database update` para aplicar las migraciones y crear la base de datos.
4. Abre una terminal y navega hasta el directorio del proyecto.
5. Ejecuta el comando `dotnet run --project APIC_Tasks.Api` para iniciar la aplicación.
6. ¡La API estará lista para usarse en http://localhost:3306!

**¿Cómo probar la API?**

Puedes utilizar herramientas como Postman o cURL para interactuar con la API. Algunos ejemplos de endpoints:

- `POST /api/tasks`: Crea una nueva tarea.
- `GET /api/tasks/{id}`: Obtiene una tarea específica por su ID.
- `PUT /api/tasks/{id}`: Actualiza una tarea existente.
- `DELETE /api/tasks/{id}`: Elimina una tarea específica.

Script DB:
```
Create database A3LAB3DanielLopezSD3;
use A3LAB3DanielLopezSD3;

CREATE TABLE Tasks (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    Description TEXT,
    CreationDate DATETIME NOT NULL,
    DueDate DATETIME NOT NULL,
    IsCompleted BOOLEAN DEFAULT FALSE
);
```


